﻿using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace RestaurantFindMany;

public class FindMany
{
    private static IMongoCollection<Restaurant>? restaurantsCollection;
    private const string MongoConnectionString = "mongodb+srv://spravce:JYHoYzXaRFMvZah5@learn-mongodb-cluster.lkbhbsw.mongodb.net";

    public static void Main(string[] args)
    {
        Setup();

        if (restaurantsCollection == null)
        {
            Environment.Exit(0);
        }

        // Find multiple documents using builders
        Console.WriteLine("Finding documents with builders...:");
        var restaurants = FindMultipleRestaurantsBuilderSync();
        Console.WriteLine($"Number of documents found: {restaurants.Count}");

        // Extra space for console readability 
        Console.WriteLine();

        // Find multiple documents using LINQ
        Console.WriteLine("Finding documents with LINQ...:");
        restaurants = FindMultipleRestaurantsLinqSync();
        Console.WriteLine($"Number of documents found: {restaurants.Count}");

        Console.WriteLine();

        // Find all restaurants
        // builders
        Console.WriteLine("Finding all documents with builders...:");
        restaurants = FindAllRestaurantsBuilderSync();
        Console.WriteLine($"Number of documents found: {restaurants.Count}");

        Console.Write(Environment.NewLine);

        // linq
        Console.WriteLine("Finding all documents with Linq...:");
        restaurants = FindAllRestaurantsLinqSync();
        Console.WriteLine($"Number of documents found: {restaurants.Count}");

        Console.Write(Environment.NewLine);

    }

    // builders
    public static List<Restaurant> FindMultipleRestaurantsBuilderSync()
    {
        // start-find-builders-sync
        FilterDefinition<Restaurant> filter = Builders<Restaurant>.Filter
            .Eq("cuisine", "Pizza");

        return restaurantsCollection.Find(filter).ToList();
        // end-find-builders-sync
    }

    // linq
    public static List<Restaurant> FindMultipleRestaurantsLinqSync()
    {
        // start-find-linq-sync
        return restaurantsCollection.AsQueryable()
            .Where(r => r.Cuisine == "Pizza").ToList();
        // end-find-linq-sync
    }

    // builders
    private static List<Restaurant> FindAllRestaurantsBuilderSync()
    {
        // start-find-all-sync
        var filter = Builders<Restaurant>.Filter.Empty;

        return restaurantsCollection.Find(filter)
            .ToList();
        // end-find-all-sync
    }

    private static List<Restaurant> FindAllRestaurantsLinqSync()
    {
        // start-find-linq-sync
        return restaurantsCollection.AsQueryable()
            .ToList();
        // end-find-linq-sync
    }

    private static void Setup()
    {
        // This allows automapping of the camelCase database fields to our models. 
        ConventionPack camelCaseConvention = new ConventionPack { new CamelCaseElementNameConvention() };
        ConventionRegistry.Register("CamelCase", camelCaseConvention, type => true);

        // Establish the connection to MongoDB and get the restaurants database
        IMongoClient mongoClient = new MongoClient(MongoConnectionString);
        IMongoDatabase restaurantsDatabase = mongoClient.GetDatabase("sample_restaurants");
        restaurantsCollection = restaurantsDatabase.GetCollection<Restaurant>("restaurants");
    }
}
